@extends('layouts.app')


@section('content')
<div class="primary-slider">
    <div class="primary-banner">
        <img src="/img/Banner-text.svg" alt="Banner text" class="banner">
    </div>
    <primary-slider-component img='/img/Banner1.png'></primary-slider-component>
</div>
    <div class="localizare wrapper-block">
        <h3 class="block-title">LOCALIZARE</h3>
        <localizare-block-component></localizare-block-component>
    </div>
    <div class="despre wrapper-block ">
        <h4 class="block-title">
            DESPRE PROIECT
        </h4>
        <info-block-component
            index='1'
            img='/img/Complex-rezidential.png'
            title='Complex rezidențial exclusivist construit de NUSCO imobiliara'
            description='Pe o suprafață de 23 de hectare, Nusco City va acomoda un total
            de peste 3.000 de apartamente, parcuri, zone verzi și infrastructură –
            care vor acoperi aproximativ 40% din întreaga suprafață a proiectului,
            unități de învățământ, zone comerciale dedicate, dar și zone de birouri.'
            >
        </info-block-component>
        <info-block-component
            index='2'
            img='/img/Inspirat-de-placerea.png'
            title='Inspirat de plăcerea pură și definit de un stil elegant,
            dar in același timp eficient'
            description='Am gândit în detaliu fiecare element: finisaje excepționale,
            compartimentări eficiente, suprafețe vitrate largi, 2500mp de
            spații verzi amenajate spectaculos cu plante înalte și copaci,
            străzi și alei interioare largi, terase generoase, locuri de joacă
            pentru copii, locuri de parcare pentru biciclete.Totul la superlativ
            pentru casa ta.'
        >
        </info-block-component>
        <info-block-component
            index='3'
            img='/img/Un-Complex-pentru.png'
            title='Un complex pentru o comunitate specială, de oameni frumoși'
            description='Un concept gândit pentru opțiunea casei tale sau pentru prietenii tăi,
            pe care ți-i dorești aproape. Un concept gândit pentru opțiunea casei tale sau pentru prietenii tăi,
            pe care ți-i dorești aproape. O comunitate durabila si responsabila,
            un mediu eficient pentru relaxarea adultilor si dezvoltarea armonioasa
            a copiilor.Intră în contact cu noi și vei primi o soluție personalizată pentru tine,
            familie și prietenii apropiați.'
        >
        </info-block-component>
    </div>
    <advantages-block-component></advantages-block-component>
    <div class="apartament wrapper-block">
        <h4 class="block-title">
            APARTAMENTE
        </h4>
        <apartament-details-component  title='Studiouri FAZA 1a' existence='false'></apartament-details-component>
        <apartament-details-component title='3 Camere - Schițe 3A, 3B, 3C' existence='true'></apartament-details-component>
        <apartament-details-component  title='2 Camere - Schițe 2A, 2B, 2C' existence='true'></apartament-details-component>
        <apartament-details-component  title='4 Camere - Schița 4A - Faza 1a' existence='false'></apartament-details-component>
    </div>
    <div class="row preview-photo wrapper-block d-flex justify-content-between">
        <div class="col-4 p-0">
            <h4 class="preview-title">Poze interioare ap. 2 camere</h4>
            <primary-slider-component img='/img/Poze-ap-2-cam.png' autoplay='false'></primary-slider-component>
        </div>
        <div class="col-4 p-0">
            <h4 class="preview-title">Poze interioare ap. 3 camere</h4>
            <primary-slider-component img='/img/Poze-ap-3-cam.png' autoplay='false'></primary-slider-component>
        </div>
        <div class="col-4 p-0">
            <h4 class="preview-title">Poze interioare ap. 4 camere</h4>
            <primary-slider-component img='/img/Poze-ap-4-cam.png' autoplay='false'></primary-slider-component>
        </div>
    </div>
    <div class="row mt-5 preview-video wrapper-block d-flex justify-content-between">
        <div class="col-4 p-0">
            <h4 class="preview-title">tur virtual ap. 2 camere</h4>
            <preview-video-component img='/img/RDU_7555-681x681-1.png'></preview-video-component>
        </div>
        <div class="col-4 p-0">
            <h4 class="preview-title">tur virtual ap. 3 camere</h4>
            <preview-video-component img='/img/RDU_7561-681x681-1.png'></preview-video-component>
        </div>
        <div class="col-4 p-0">
            <h4 class="preview-title">tur virtual ap. 4 camere</h4>
            <preview-video-component img='/img/RDU_7510-681x681-1.png'></preview-video-component>
        </div>
    </div>
    <div class="wrapper-block finish-block">
        <h3 class="block-title">FINISAJE</h3>
        <info-block-component
            index='1'
            img='/img/Structura.png'
            title='STRUCTURA'
            description='Infrastructura este realizată în sistem fundație tip radier general
            cu grosimea de 70cm sub peretii structurali din beton armat. Suprastructura este
            alcatuită din diafragme și grinzi din beton armat, planșeele având grosimea de 17cm.
            La alcătuirea și calculul structurii s-a urmărit realizarea condițiilor de rezistență,
            deformabilitate și stabilitate impuse de normativul de protectie antiseismică.'
        >
        </info-block-component>
        <info-block-component
            index='2'
            img='/img/Inchideri.png'
            title='ÎNCHIDERI ȘI FINISAJE EXTERIOARE'
            description='Închiderile exterioare sunt realizate din zidărie de cărămidă cu goluri verticale,
            eficientă termic – tip Porotherm – având 30cm grosime, peste care se montează un termosistem
            specific din PEX de 10 cm grosime. Pentru stratul finit exterior se va executa o tencuială
            decorativă culoare alb. Doar la nivelul parterului, precum și pentru intradosul balcoanelor,
            se va realiza un placaj uscat – imitatie lemn – Trespa Olanda.'
        >
        </info-block-component>
        <info-block-component
            index='3'
            img='/img/Compartimentari.png'
            title='COMPARTIMENTĂRI INTERIOARE'
            description='Compartimentările interioare dintre apartamente, precum și compartimentările
            dintre apartamente și spațiile comune, se vor realiza din zidărie de cărămidă cu
            goluri verticale – tip Porotherm – având 30cm grosime. Pereții de compartimentare
            interiori apartamentelor vor fi din zidărie de cărămidă de 15cm grosime, respectiv
            30cm grosime. Tencuielile interioare ale apartamentelor vor fi de tip uscat ,
            peste care se va aplica un strat de vopsitorie lavabilă, culoare alb.'
        >
        </info-block-component>
        <info-block-component
            index='4'
            img='/img/Timplarie-ext.png'
            title='TÂMPLARIE EXTERIOARĂ'
            description='Tâmplarie aluminiu – profil SCHÜCO, sistem AWS , 70 High Insulation,
            culoare profile gri antracit, cu geamuri duplex termoizolante 4 Seasons (6mm – 18Ar – 4mm).
            Coeficient de izolare termică Uf = 1.5W/mpK, coeficient de izolare fonică până la 35dB,
            funcționalitate pe termen lung clasa 3. Feronerie SCHÜCO AvantTech.'
        >
        </info-block-component>
        <info-block-component
            index='5'
            img='/img/Timplarie-int.png'
            title='TÂMPLĂRIE INTERIOARĂ'
            description='Uși blindate de intrare apartamente – Alias Silver C Italia – structura metalică,
            prag metalic retractabil automat la deschiderea ușii, clasa 3 antiefracție,
            transfer termic 1.3W/mpK , izolație fonică 34db, sistem de închidere cu cilindru
            european în trei puncte, sistem de balamale ascunse, feronerie cromată.
            Uși interioare – model Pinum Suite Plus – alcătuite pe structura celulară,
            cu panouri exterioare HDF, finisaj CPL, culoare RAL9010 (culoare alb).
            Usa este echipată cu balamale ascunse AGB și cu broasca magnetică.'
        >
        </info-block-component>
        <info-block-component
            index='6'
            img='/img/Finisaje-parchet.png'
            title='FINISAJE PARDOSELI – PARCHET'
            description='Parchet triplustrasificat – seria 2500 – Plank 1
            Strip – finisaj stejar – dim. 12*180*2200mm – rezistență la alunecare
            R9/R10 – HARO – Germania . Potrivit pentru sistemul de încălzire în pardoseală.
            Finisajul cu parchet se va realiza în zona de living-room și dormitoare.'
        >
        </info-block-component>
        <info-block-component
            index='7'
            img='/img/Finisaje-placi-ceramice.png'
            title='FINISAJE PARDOSELI – PLĂCI CERAMICE'
            description='Plăci ceramice, gresie porțelanată rectificată mată – dimensiuni
            59.2*59.2cm – model Logan Bianco – Cristacer Spania. Finisaj potrivit pentru
            sistemul de încălzire în pardoseală.'
        >
        </info-block-component>
        <info-block-component
            index='8'
            img='/img/Finisaje-pereti-bai.png'
            title='FINISAJE PEREȚI BAI – PLĂCI CERAMICE'
            description='Plăci ceramice, gresie porțelanată rectificată mată – dimensiuni
            29.2*59.2cm – model Logan Bianco – Cristacer Spania. Toate băile vor
            beneficia de un design special, prin introducerea unor
            plăci de decor cu dimensiuni mari 60*120cm.'
        >
        </info-block-component>
        <info-block-component
            index='9'
            img='/img/Obiecte-sanitare.png'
            title='OBIECTE SANITARE'
            description='Băile vor fi complet utilate cu obiecte sanitare de înaltă
            calitate – vas WC suspendat, lavoar montat pe blat de lemn din stejar
            stratificat, oglinda, cabina duș/cadă, baterii lavoar/cadă – produse
            de către Ideal Standard – Italia.'
        >
        </info-block-component>
        <info-block-component
            index='10'
            img='/img/Aparataj-termal.png'
            title='APARATAJ TERMINAL'
            description='Prize și întrerupătoare – producator grup
            Legrand – Bticino – Italia – gama Classia. Linia acestei
            game urmărește un design simplu și modern.'
        >
        </info-block-component>
        <info-block-component
            index='11'
            img='/img/Sistem-de-incalzire.png'
            title='SISTEM DE ÎNCĂLZIRE ÎN PARDOSEALĂ'
            description='Încălzirea la nivel de apartament se realizează
            printr-un sistem ultramodern de încălzire în pardoseală care
            asigură un confort optim prin distribuirea căldurii pe toata
            suprafața încăperii, un mediu sănătos prin reducerea cantității
            de praf, precum și economie de energie. Producator Rehau.'
        >
        </info-block-component>
        <info-block-component
            index='12'
            img='/img/Centrala-Termica.png'
            title='CENTRALA TERMICĂ'
            description='Fiecare apartament va fi dotat cu centrală termică în
            condensație – Viessman – Vitodens – 24kW – cu tiraj forțat – cu afisaj LCD.
            Schimbătorul de căldură de înaltă calitate, transformă eficient energia
            utilizată în căldură, precum și furnizarea rapidă a apei la temperatura
            constantă și debit dorit. Clasa de eficiență energetică A.
            Centrala va fi murală și montată în bucătărie.'
        >
        </info-block-component>
        <info-block-component
            index='13'
            img='/img/Gradina-Interioara.png'
            title='GRADINA INTERIOARA SPECTACULOASA'
            description='Am gândit grădina interioară de la Nusco City drept o compoziție organică
            de vegetație, cu jocuri de apă și elemente inspirate din grădinile japoneze.
            Vei putea admira un spectacol de culori și texturi ce se va schimba în
            funcție de anotimp, pentru a crea atmosfera potrivită.
            Conceptul de spațiu este definit prin linii curbe și valuri ce se regăsesc
            în formele aleelor din corten și stratificație de pământ. Grădina este
            construită după un concept de amfiteatru, care să joace rolul de loc de
            relaxare pentru toți membrii comunității'
        >
        </info-block-component>
    </div>
    <div class="wrapper-block">
        <h3 class="block-title">Stadiul Lucrărilor</h3>
        <div class="d-flex justify-content-between">
            <div class="col-6 p-0">
                <h4 class="preview-title">FAZA 2A – IUNIE 2023</h4>
                <primary-slider-component img='/img/IMG_9901-1.png' autoplay='false'></primary-slider-component>
            </div>
            <div class="col-6 p-0">
                <h4 class="preview-title">FAZA 2B – OCTOMBRIE 2023</h4>
                <primary-slider-component img='/img/Faza-2B.png' autoplay='false'></primary-slider-component>
            </div>
        </div>
    </div>
    <div class="wrapper-block about-developer row d-flex direction-column justify-content-center">
        <div class="grey-line"></div>
        <div class="row p-0 d-flex justify-content-between">
            <div class="col-6 p-0 left-bar">
                <h4 class="title mt-5">Despre dezvoltator</h4>
                <p class="description mt-4">
                    Recunoscut ca unul dintre cei mai mari și mai de încredere dezvoltatori din România,
                    grupul Nusco funcționează pe piața românească încă din anul 1997. Încă de atunci,
                    familia Nusco s-a impus atât prin planurile îndrăznețe, cât și prin dorința
                    de a face întotdeauna lucrurile bine, fără grabă, cu atenție și cu respect
                    pentru nevoile și dorințele românilor. Fiecare proiect dezvoltat de grupul
                    Nusco este emblema și garanția calității dezvoltatorului, ale cărui planuri
                    pe termen foarte lung în România vor continua într-un ritm susținut.
                </p>
            </div>
            <div class="col-6 p-0 right-bar">
                <img src="/img/Despre-Dezvoltator.png" alt="Dezvoltator">
            </div>
        </div>
    </div>
    <div class="contact-form">
        <div class="wrapper-block">
            <contact-form-component></contact-form-component>
        </div>
    </div>
@endsection

