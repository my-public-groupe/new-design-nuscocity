<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home Page</title>
    @vite(['resources/css/bootstrap.css'])
    @vite(['resources/css/app.css'])
</head>
<body>
    <!-- ... body ... -->
    <div id="app">
        <header-component></header-component>
        @yield('content')
        <footer class="main-footer">
            <main-footer-component></main-footer-component>
        </footer>
    </div>
    <!-- ... scripts ... -->
    @vite('resources/js/app.js')
</body>
</html>
