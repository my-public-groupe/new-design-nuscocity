import 'bootstrap';
import { createApp } from 'vue';
import HeaderFaza2 from './src/components/HeaderFaza2.vue';
import PrimarySlider from './src/components/PrimarySlider.vue';
import LocalizareBlock from './src/components/LocalizareBlock.vue';
import InfoBlock from './src/components/InfoBlock.vue';
import AdvantagesBlock from './src/components/AdvantagesBlock.vue';
import ApartamentDetails from './src/components/ApartamentDetails.vue';
import PreviewVideo from './src/components/PreviewVideo.vue';
import ContactForm from './src/components/ContactForm.vue';
import MainFooter from './src/components/MainFooter.vue';

const app = createApp({});

app.component('header-component', HeaderFaza2);
app.component('primary-slider-component', PrimarySlider);
app.component('localizare-block-component', LocalizareBlock);
app.component('info-block-component', InfoBlock);
app.component('advantages-block-component', AdvantagesBlock);
app.component('apartament-details-component', ApartamentDetails);
app.component('preview-video-component', PreviewVideo);
app.component('contact-form-component', ContactForm);
app.component('main-footer-component', MainFooter);


app.mount("#app");
